#!/bin/sh
# shellcheck disable=SC2059

set -e

force_reset() {
  git reset --hard origin/master
  git pull --depth=1
}

clone_or_update_repo() {
  line="$*"
  cd "$HOME/.bin/.repos" || exit 1

  url="$(printf "$line" | awk '{print $1}')"
  repo="$(printf "$line" | awk '{print $2}')"
  basename="$(dirname "$url" | sed -e 's/^http:\/\///g' -e 's/^https:\/\///g' )"

  mkdir -p "$PWD/$basename"

  if [ ! -d "$PWD/$basename/$repo" ]
  then
    git_version="$(git version | rev | awk '{print $1}' | awk -F'.' '{print $1}')"
    [ "$git_version" -gt 1 ] && shallow_submodules="--shallow-submodules" || shallow_submodules=""
    git clone --recursive --depth 1 $shallow_submodules "$url" "$PWD/$basename/$repo" || return
  else
    cd "$PWD/$basename/$repo" || return
    printf "Updating %s\\n" "$url"
    git fetch --all
    git pull --depth 1 || force_reset || return
  fi
}

if [ -n "$1" ]
then
  clone_or_update_repo "$@"
  exit "$?"
fi

REPOS="
  https://github.com/maandree/nightshift.git nightshift
  https://github.com/robbyrussell/oh-my-zsh.git oh-my-zsh
  https://gitlab.com/ahrs/demoji.git demoji
  https://gitlab.com/ahrs/soundcloudmpd.git soundcloudmpd
  https://github.com/pipeseroni/pipes.sh.git pipes.sh
  https://github.com/alexanderjeurissen/ranger_devicons ranger_devicons
  https://github.com/jschx/ufetch ufetch
  https://github.com/stark/Color-Scripts color-scripts
  https://github.com/alexherbo2/xdotool-mouse-window xdotool-mouse-window
"

mkdir -p "$HOME/.bin/.repos" || exit 1

command -v parallel > /dev/null 2>&1 && HAS_PARALLEL=1 || HAS_PARALLEL=0

if [ "$HAS_PARALLEL" -eq 0 ]
then
  printf "$REPOS" | while read -r line
  do
    [ ! -n "$line" ] && continue
    clone_or_update_repo "$line"
  done
else
  printf "%s" "$REPOS" | parallel -r "$0 {}"
fi
