# vim: filetype=sh
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
[ -n "$ZSH_VERSION" ] && export HISTFILE="$ZDOTDIR/.zsh_history"

export PYTHONSTARTUP="$XDG_CONFIG_HOME/pythonrc"

# ensure we always start with a default PATH instead of exponentially appending to $PATH each time ~/.profile is sourced
unset PATH
export PATH="$(/bin/sh -c 'unset PATH;[ ! -f /etc/profile ] && exit 1;. /etc/profile > /dev/null 2>&1;printf "%s" "$PATH"')"

# sensible default if the above fails for some reason
[ -z "$PATH" ] && export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/bin"

export PATH="$PATH:/usr/bin"

export PATH="$HOME/.bin:$HOME/.bin/firejail:$PATH"
[ -d /snap/bin ] && export PATH="$PATH:/snap/bin"
[ -d "$XDG_DATA_HOME/n" ] && export N_PREFIX="$XDG_DATA_HOME/n" && export PATH="$XDG_DATA_HOME/n/bin:$PATH"

[ -d /usr/local/share/i18n/locales/musl ] && export MUSL_LOCPATH=/usr/local/share/i18n/locales/musl

[ -d "$HOME/.local/bin" ] && export PATH="$PATH:$HOME/.local/bin"

command -v node > /dev/null 2>/dev/null  && export PATH="$PATH:$HOME/.yarn/bin"
command -v ruby > /dev/null 2>/dev/null  && export PATH="$PATH:$(ruby -e 'print Gem.user_dir')/bin"
command -v go > /dev/null 2>/dev/null    && export GOPATH="$HOME/.go" && export PATH="$PATH:$GOPATH/bin"
[ -f "$HOME/.cargo/bin/rustup" ] && export PATH="$PATH:$HOME/.cargo/bin" && export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/src"

# Add random scripts to $PATH from repos I've cloned in ~/.bin/.repos (this may produce some false-positives but there's no harm having them in $PATH)
# NOTE: this won't work for BSD find (see: https://stackoverflow.com/questions/4458120/unix-find-search-for-executable-files#4458361) for an alternative
eval "$(find ~/.bin/.repos -maxdepth 4 -type d -name "bin" | while read -r line; do export PATH="$PATH:$line";echo "export PATH='$PATH'"; done)"
eval "$(/usr/bin/env -i find ~/.bin/.repos -maxdepth 4 -type f -executable | xargs -I'{}' dirname {} | sort -u | while read -r line; do echo "export PATH=\"\$PATH:$line\""; done)"

command -v cygpath > /dev/null 2>&1 && command -v powershell > /dev/null 2>&1 && [ -f ~/dotfiles/get-winblows-path.ps1 ] && eval "$(powershell ~/dotfiles/get-winblows-path.ps1  | xargs -d';' -r -I'{}' cygpath -u {} | while read -r line; do [ -z "$line" ] && continue; printf "export PATH=\"\$PATH:$line\";"; done)"

# I define TERMINAL, EDITOR, VISUAL and BROWSER in scripts inside
# ~/.bin. This allows me to dynamically change them without having to update environment variables
# I still perform some sanity checks to see if the programs specified are installed though and
# if not unset the variables.

. ~/.bin/terminal
if [ -f "$TERMINAL" ]; then
  export TERMINAL="$HOME/.bin/terminal"
else
  unset TERMINAL
fi

. ~/.bin/browser
if [ -f "$BROWSER" ]; then
  export BROWSER="$HOME/.bin/browser"
else
  unset BROWSER
fi

. ~/.bin/editor
if [ -f "$EDITOR" ]; then
  export EDITOR="$HOME/.bin/editor"
else
  unset EDITOR
fi

if [ -f "$VISUAL" ]; then
  export VISUAL="$HOME/.bin/editor"
else
  unset VISUAL
fi

if [ ! -f "$XDG_CONFIG_HOME/user-dirs.dirs" ]; then
    XDG_DESKTOP_DIR="$HOME/Desktop"
    XDG_DOWNLOAD_DIR="$HOME/Downloads"
    XDG_TEMPLATES_DIR="$HOME/"
    XDG_PUBLICSHARE_DIR="$HOME/Public"
    XDG_DOCUMENTS_DIR="$HOME/Documents"
    XDG_MUSIC_DIR="$HOME/Music"
    XDG_PICTURES_DIR="$HOME/Pictures"
    XDG_VIDEOS_DIR="$HOME/Videos"
else
  . "$XDG_CONFIG_HOME/user-dirs.dirs"
fi

export XDG_DESKTOP_DIR
export XDG_DOWNLOAD_DIR
export XDG_TEMPLATES_DIR
export XDG_PUBLICSHARE_DIR
export XDG_DOCUMENTS_DIR
export XDG_MUSIC_DIR
export XDG_PICTURES_DIR
export XDG_VIDEOS_DIR

if [ -z "$TERMINAL" ]; then export TERMINAL="$(command -v -p xterm)"; fi
if [ -z "$BROWSER" ]; then export BROWSER="$(command -v -p firefox)"; fi
if [ -z "$EDITOR" ]; then export EDITOR="$(command -v -p vi)"; fi
if [ -z "$VISUAL" ]; then export VISUAL="$EDITOR"; fi
if [ -f "$XDG_CONFIG_HOME/hidpirc" ]; then . "$XDG_CONFIG_HOME/hidpirc"; fi
