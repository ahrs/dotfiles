"----------------------------------------"
"           ▄▄               ▄           "
"         ▄▄▄▄▄▄             ▄▄▄         "
"       ▄▄▄▄▄▄▄▄▄            ▄▄▄▄▄       "
"     ▄▄▄▄▄▄▄▄▄▄▄▄           ▄▄▄▄▄▄▄     "
"    ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄         ▄▄▄▄▄▄▄▄    "
"    ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄        ▄▄▄▄▄▄▄▄    "
"    ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄       ▄▄▄▄▄▄▄▄    "
"    ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄      ▄▄▄▄▄▄▄▄    "
"    ▄▄▄▄▄▄▄▄▄▀▄▄▄▄▄▄▄▄▄▄    ▄▄▄▄▄▄▄▄    "
"    ▄▄▄▄▄▄▄▄▄ ▀▄▄▄▄▄▄▄▄▄▄   ▄▄▄▄▄▄▄▄    "
"    ▄▄▄▄▄▄▄▄▄  ▀▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄    "
"    ▄▄▄▄▄▄▄▄▄    ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄    "
"    ▄▄▄▄▄▄▄▄▄     ▀▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄    "
"    ▄▄▄▄▄▄▄▄▄      ▀▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄    "
"    ▄▄▄▄▄▄▄▄▄        ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄    "
"    ▄▄▄▄▄▄▄▄▄         ▀▄▄▄▄▄▄▄▄▄▄▄▄▄    "
"     ▀▀▄▄▄▄▄▄          ▀▄▄▄▄▄▄▄▄▄▄▀     "
"        ▀▄▄▄▄            ▄▄▄▄▄▄▄▀       "
"         ▀▀▄▄             ▀▄▄▄▀         "
"            ▀              ▀▀           "
"                                        "
"----------------------------------------"

"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" explicitely disable modelines
set modelines=0
set nomodeline
set noshowmode

let mapleader = ","

" Required:
set runtimepath+=$HOME/.vim/repos/github.com/Shougo/dein.vim

" Workaround distro's that don't ship neovim plugins by explicitely
" sourcing the individual plugins I'm interested in. I don't want
" to set the runtimepath as this could cause issues.
if has('nvim')
  if ! filereadable("/usr/share/nvim/runtime/plugin/fzf.vim")
    if filereadable("/usr/share/vim/vimfiles/plugin/fzf.vim")
      source /usr/share/vim/vimfiles/plugin/fzf.vim
    endif
  endif
endif

" Debian's fzf package doesn't add fzf.vim to vims runtimepath
if filereadable("/usr/share/doc/fzf/examples/fzf.vim")
  source /usr/share/doc/fzf/examples/fzf.vim
endif

" Required:
if dein#load_state('~/.vim')
  call dein#begin('~/.vim')

  " Let dein manage dein
  " Required:
  call dein#add('~/.vim/repos/github.com/Shougo/dein.vim')

  if system("command -v ebuild") != ""
      call dein#add('gentoo/gentoo-syntax')
  endif

  call dein#add('Shougo/neosnippet.vim')
  call dein#add('Shougo/neosnippet-snippets')

  call dein#add('tpope/vim-commentary')

  call dein#add('Glench/Vim-Jinja2-Syntax')

  if system("command -v ctags") != '' || system("command -v global") != ''
    call dein#add('majutsushi/tagbar')
    call dein#add('xolox/vim-misc')
    call dein#add('xolox/vim-easytags')
  endif

  " You can specify revision/branch/tag.
  " call dein#add('Shougo/vimshell', { 'rev': '3787e5' })

  " Colour Schemes
  call dein#add('dylanaraps/wal.vim')
  call dein#add('arcticicestudio/nord-vim')

  " Rust
  call dein#add('rust-lang/rust.vim')
  call dein#add('racer-rust/vim-racer')

  " Golang
  call dein#add('google/vim-ft-go')

  " SaltStack
  call dein#add('saltstack/salt-vim')

  " Web Dev Plugins
    " Pug Syntax Highlighting
    call dein#add('digitaltoad/vim-pug')
    " Emmet-vim
    call dein#add('mattn/emmet-vim')
    " React JSX Syntax Highlighting
    call dein#add('mxw/vim-jsx')
    " JSHint
    call dein#add('walm/jshint.vim')
    " Vim plugin for XO
    call dein#add('sindresorhus/vim-xo')
    " Syntax Highlighting for Vue.js
    call dein#add('posva/vim-vue')

  " Git Plugins
  call dein#add('tpope/vim-fugitive')
  call dein#add('itchyny/vim-gitbranch')
  call dein#add('gregsexton/gitv')

  " PowerShell support ;)
  call dein#add('PProvost/vim-ps1')

  call dein#add('berdandy/AnsiEsc.vim')

  " Misc Plugins
    " Open PDF Files in Vim
    call dein#add('rhysd/open-pdf.vim')
    " Auto-format code
    call dein#add('Chiel92/vim-autoformat')

    " Asynchronous linting and make framework for Neovim/Vim
    call dein#add('neomake/neomake')

  " These are plugins for neovim that obviously
  " won't work in vanilla vim.
  if has('nvim')
    call dein#add('ncm2/ncm2')

    " Complete other buffers
    call dein#add('fgrsnau/ncm-otherbuf')

    " C/C++ completion
    call dein#add('roxma/ncm-clang')

    " CSS completion
    call dein#add('calebeby/ncm-css')

    " Nvim client for GhostText (Firefox add-on)
    call dein#add('raghur/vim-ghost')

    " Wrapper for neovim's :terminal functions
    call dein#add('kassio/neoterm')

    " Neovim-GUI Shim
    call dein#add('equalsraf/neovim-gui-shim')
  endif

  "i3 Config Syntax Highlighting
  if filereadable($HOME . "/.config/i3/config")
    call dein#add('mboughaba/i3config.vim')
    autocmd BufRead ~/.config/i3/config :setlocal modelines=1 | :setlocal modeline
  endif

  " XClip
  call dein#add('erickzanardo/vim-xclip')

  if $TMUX != ""
    " Tmux Integration
    call dein#add('benmills/vimux')
    call dein#add('tmux-plugins/vim-tmux')
    autocmd BufRead ~/.tmux.conf.local :setlocal modelines=1 | :setlocal filetype=tmux
  endif

  call dein#add('vimwiki/vimwiki')

  if system("command -v zeal") != ""
    call dein#add('KabbAmine/zeavim.vim')
  endif

  if system("command -v dasht") != ""
    call dein#add('sunaku/vim-dasht')
  endif

  if system("command -v fzf") != ""
    " Fuzzy Search
    call dein#add('junegunn/fzf.vim')
  endif

  if system("command -v ranger") != ""
    if has('nvim')
      call dein#add('Lokaltog/neoranger')
    else
      call dein#add('francoiscabrol/ranger.vim')
    endif
  endif

  if has('python')
    if system("command -v editorconfig") != ""
      " EditorConfig (loaded last to prevent anything conflicting)
      call dein#add('editorconfig/editorconfig-vim')
    endif
  endif

  call dein#add('tlvince/securemodelines')

  call dein#add('itchyny/lightline.vim')

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif

"End dein Scripts-------------------------

filetype plugin on

if $LC_TERM_PROFILE == "light"
  set background=light
else
  set background=dark
endif

if $VIM_COLOR_SCHEME != ""
  colorscheme $VIM_COLOR_SCHEME
else
  colorscheme default
endif

" Disable the easytags version warning
let g:easytags_suppress_ctags_warning = 1

" Editorconfig
let g:EditorConfig_exec_path = '/usr/bin/editorconfig'
let g:EditorConfig_core_mode = 'external_command'

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

let g:dasht_filetype_docsets = {}

" When in Elixir, also search Erlang:
let g:dasht_filetype_docsets['elixir'] = ['erlang']

" When in C++, also search C, Boost, and OpenGL:
let g:dasht_filetype_docsets['cpp'] = ['^c$', 'boost', 'OpenGL']

" When in Python, also search NumPy, SciPy, and Pandas:
let g:dasht_filetype_docsets['python'] = ['(num|sci)py', 'pandas']

" When in HTML, also search CSS, JavaScript, Bootstrap, and jQuery:
let g:dasht_filetype_docsets['html'] = ['css', 'js', 'bootstrap']

let g:dasht_filetype_docsets['sls'] = ['saltstack']
"Dasht tries to search a non-existent "sls" docset
autocmd BufEnter *.sls set filetype+=.saltstack
"Dasht doesn't search nodejs docsets when editing .js files
autocmd BufEnter *.js set filetype+=.node

" Neomake - Advanced Setup
" https://github.com/neomake/neomake#advanced-setup
function! MyOnBattery()
  if filereadable('/sys/class/power_supply/AC/online')
    return readfile('/sys/class/power_supply/AC/online') == ['0']
  else
    return 1
  endif
endfunction

if MyOnBattery()
  call neomake#configure#automake('w')
else
  call neomake#configure#automake('nw', 1000)
endif

" Map semicolon to colon
nnoremap ; :
vnoremap ; :

" I don't know why I added this or if I still need it!
" Searching brings up this:
" http://vim.wikia.com/wiki/Make_Vim_completion_popup_menu_work_just_like_in_an_IDE
" To Do:
"   * Find out if I still need this or not ;)
" smart tab for auto complete
inoremap <expr> <silent> <Tab>  pumvisible()?"\<C-n>":"\<TAB>"
inoremap <expr> <silent> <S-TAB>  pumvisible()?"\<C-p>":"\<S-TAB>"

" Plugin key-mappings.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif

" VIM: disabling the cursor/arrow keys, but only for navigation - https://stackoverflow.com/a/18805680
" Disable Arrow keys in Escape mode
"map <up> <nop>
"map <down> <nop>
map <left> <nop>
map <right> <nop>

"https://stackoverflow.com/a/6053341
" Use ctrl-[hjkl] to select the active split!
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-l> :wincmd l<CR>

" https://stackoverflow.com/questions/3878692/aliasing-a-command-in-vim#3879737
fun! SetupCommandAlias(from, to)
  exec 'cnoreabbrev <expr> '.a:from
        \ .' ((getcmdtype() is# ":" && getcmdline() is# "'.a:from.'")'
        \ .'? ("'.a:to.'") : ("'.a:from.'"))'
endfun
call SetupCommandAlias("W","w")
call SetupCommandAlias("Wq","wq")

map <silent><leader>ln :set number! relativenumber!<cr>

" Exit Neovim's terminal mode
" https://vi.stackexchange.com/questions/4919/exit-from-terminal-mode-in-neovim
tnoremap <Esc> <C-\><C-n>

let s:color_column_old = 60
function! s:ToggleColorColumn()
    if s:color_column_old == 0
        let s:color_column_old = &colorcolumn
        set colorcolumn=0
    else
        let &colorcolumn=s:color_column_old
        let s:color_column_old = 0
    endif
endfunction
nnoremap <silent><leader>ll :call <SID>ToggleColorColumn()<cr>

let g:lightline = {
            \ 'colorscheme': 'nord',
            \ 'component': {
            \   'lineinfo': ' %3l:%-2v',
            \   'gitbranch': ' %{gitbranch#name()}',
            \ },
            \ 'component_expand' : {
            \  'linter_checking': 'Checking',
            \  'linter_warnings': 'Warnings',
            \  'linter_errors': 'Errors',
            \ },
            \ 'active': {
            \   'left': [ [ 'mode', 'paste' ],
            \              [ 'gitbranch' ],
            \             [ 'filename', 'readonly', 'modified' ] ],
            \    'right': [  [ 'percent', 'lineinfo' ],
            \                 [ 'linter_checking','linter_errors', 'linter_warnings' ],
            \               [ 'fileformat', 'fileencoding', 'filetype' ] ]
            \ },
            \ 'inactive' : {
            \     'right': [ [ 'lineinfo' ] ]
            \ }
            \ }

" Use the mouse!
set mouse=a
set number

" Set formatting options last so that it's not immediately
" overriden by any other plugins.
set tabstop=4
set shiftwidth=2
set softtabstop=0
set expandtab
set nocursorline

if has('nvim')
  if exists('g:GtkGuiLoaded')
    call rpcnotify(1, 'Gui', 'Font', 'BlexMono Nerd Font Mono 13')
  endif
endif

cmap w! w !SUDO_ASKPASS='/usr/bin/ksshaskpass' sudo -A tee % > /dev/null<CR>

call SetupCommandAlias("W!", "w!")

autocmd BufWrite ~/.vimrc :source ~/.vimrc
