from ranger.api.commands import Command

class set_image_preview_method(Command):
    def execute(self):
        from os import environ
        from distutils.spawn import find_executable

        term = environ.get('TERM')
        try:
            terminology = int(environ.get('TERMINOLOGY') or '0')
        except:
            terminology = 0

        if term.endswith('kitty') or ((environ.get('TERMINFO') or '/dev/null')).split('/')[-2] == 'kitty':
            # Kitty image preview - https://github.com/ranger/ranger/pull/1077
            # NOTE: Doesn't seem to work in tmux for some reason
            self.fm.settings.preview_images_method = 'kitty'
        elif term.startswith('rxvt-unicode'):
            self.fm.settings.preview_images_method = 'urxvt'
        elif terminology == 1:
            self.fm.settings.preview_images_method = 'terminology'
        elif (environ.get('TERM_PROGRAM') or '').startswith('iTerm'):
            # Untested...
            self.fm.settings.preview_images_method = 'iterm2'
        elif not find_executable('w3m'):
            if find_executable('mpv'):
                # with mpv - https://github.com/ranger/ranger/wiki/Image-Previews#with-mpv
                self.fm.settings.preview_images_method = 'mpv'
            else:
                self.fm.settings.preview_images = False
                # Uncomment this to use a custom script, e.g for ascii previews
                # https://github.com/ranger/ranger/wiki/Image-Previews#ascii-previews
                #self.fm.settings.use_preview_script = True
                #self.fm.settings.preview_script = '~/path/to/your/scope.sh'
        else:
            self.fm.settings.preview_images_method = 'w3m'

class fzf_select(Command):
    """
    :fzf_select

    Find a file using fzf.

    With a prefix argument select only directories.

    See: https://github.com/junegunn/fzf
    """
    def execute(self):
        import subprocess
        import os.path
        if self.quantifier:
            # match only directories
            command="find -L . \( -path '*/\.*' -o -fstype 'dev' -o -fstype 'proc' \) -prune \
                    -o -type d -print 2> /dev/null | sed 1d | cut -b3- | fzf +m"
        else:
            # match files and directories
            command="find -L . \( -path '*/\.*' -o -fstype 'dev' -o -fstype 'proc' \) -prune \
                    -o -print 2> /dev/null | sed 1d | cut -b3- | fzf +m"
        fzf = self.fm.execute_command(command, stdout=subprocess.PIPE)
        stdout, stderr = fzf.communicate()
        if fzf.returncode == 0:
            fzf_file = os.path.abspath(stdout.decode('utf-8').rstrip('\n'))
            if os.path.isdir(fzf_file):
                self.fm.cd(fzf_file)
            else:
                self.fm.select_file(fzf_file)

class extract(Command):
    def execute(self):
        import subprocess
        from os.path import abspath, basename, isdir
        file_name = basename(self.fm.thisfile.path)
        index_of_dot = file_name.index('.')
        file_name_without_extension = file_name[:index_of_dot]
        dirname = abspath(file_name_without_extension.rstrip('\n'))
        command = "dtrx -q -n '" + self.fm.thisfile.path + "'"
        x = self.fm.execute_command(command, stdout=subprocess.PIPE)
        stdout, stderr = x.communicate()
        if x.returncode == 0:
            if isdir(dirname):
                self.fm.cd(dirname)


class mpv_play(Command):
    def execute(self):
        from os import system
        PATH=str(self.fm.thisdir.get_selection()[0])
        PATH.replace('"', "")
        PATH.replace('$(', "")
        PATH.replace('`', "")
        PATH='"' + PATH + '"'

        cmd='mpv --really-quiet --input-ipc-server=/tmp/mpvsocket --title=mpv '

        vid_exts = [ '.mp4', '.webm', '.flv', '.gif', '.gifv', '.mpv', '.mkv', '.ogv' ]

        if not any(ext in PATH for ext in vid_exts):
            cmd = cmd + "--no-video "

        system("pkill -9 mpv > /dev/null ; " + "nohup " + cmd + PATH + " -- > /dev/null 2>&1 &")
