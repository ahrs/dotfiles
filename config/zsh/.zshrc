if [[ $- != *i* ]] ; then
  # Shell is non-interactive.  Be done now!]
  return
fi

export XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"

[ -d "$HOME/mnt/192.168.0.11_storage/home/Music" ] && export XDG_MUSIC_DIR="$HOME/mnt/192.168.0.11_storage/home/Music"

# Set konsole TERM correctly so that things like mouse control work properly in neovim
# The default value of xterm-256color is incorrect as Konsole does not stricly adhere to
# the xterm "standard"
[[ "$(ps -o comm= $(ps -o ppid= $$))" =~ "konsole" ]] && export TERM=konsole-256color || :

#[[ -n "${commands[tmux]}" && -z "$TMUX" && ! "$(tty)" =~ "/dev/tty" ]] && { (tmux list-sessions &>  /dev/null && exec tmux attach -d) || exec tmux }

[[ -n "$TMUX" ]] && { [[ -n "$SSH_CONNECTION" ]] && tmux set status-position top || tmux set status-position bottom }

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.bin/.repos/github.com/robbyrussell/oh-my-zsh"

[[ -d /dev/shm ]] && LESSHISTFILE=/dev/shm/.lesshst || LESSHISTFILE=/tmp/.lesshst
export LESSHISTFILE

# Set GNU Parallel HOME to ~/.config/parallel
export PARALLEL_HOME="$HOME/.config/parallel"

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
if [ -z "$ZSH_THEME" ]
  then
  if [[ "$SSH_CONNECTION" ]]
  then
    ZSH_THEME_CLOUD_PREFIX=" "
    ZSH_THEME="cloud"
  else
    ZSH_THEME="refined"
  fi
fi

# ========================================
# Plugin Configuration
# ========================================
# Nothing yet...

# ========================================
# OS Specific Plugins
# ========================================
OS_PLUGINS=()

# How to check if running in Cygwin, Mac or Linux?
# https://stackoverflow.com/a/27776822
if [ ! -f "$HOME/.zsh-os-plugins" ]
then
  case "$(uname -s)" in
   Darwin)
     OS_PLUGINS+=(osx)
     if brew --version &> /dev/null
     then
      OS_PLUGINS+=(brew)
     fi
     OS_PS1="%(?.%F{magenta}.%F{red}) $(printf '\Uf302 ')%f"
     ;;

   OpenBSD)
     OS_PLUGINS+=(openbsd)
     ;;

   FreeBSD)
     OS_PS1="%(?.%F{magenta}.%F{red}) $(printf '\Uf30c ')%f"
     ;;

   Linux)
     OS="$(grep -E '^ID=' /etc/os-release | sed -e 's/^.*=//g' -e 's/\"//g')"
     OS_PS1="%(?.%F{magenta}.%F{red}) $(printf '\Uf31a ')%f"
     case "$OS" in

      debian)
        OS_PLUGINS+=(debian)
        OS_PS1="%(?.%F{magenta}.%F{red}) $(printf '\Uf306 ')%f"
        ;;
      ubuntu)
        OS_PLUGINS+=(ubuntu)
        OS_PS1="%(?.%F{magenta}.%F{red}) $(printf '\Uf31c ')%f"
        ;;
      fedora)
        OS_PLUGINS+=(fedora)
        # Use dnf --version because the fedora docker container doesn't have a `which` command
        if dnf --version &> /dev/null
        then
          OS_PLUGINS+=(dnf)
        fi
        OS_PS1="%(?.%F{magenta}.%F{red}) $(printf '\Uf30a ')%f"
        ;;
      gentoo)
        OS_PS1="%(?.%F{magenta}.%F{red}) $(printf '\Uf30d ')%f"
        ;;
      suse|opensuse)
        OS_PLUGINS+=(suse)
        OS_PS1="%(?.%F{magenta}.%F{red}) $(printf '\Uf314 ')%f"
        ;;
      *)
        OS_LIKE="$(grep -E '^ID_LIKE=' /etc/os-release | sed -e 's/^.*=//g' -e 's/\"//g')"
        case "$OS_LIKE" in
          arch|archlinux)
            OS_PLUGINS+=(archlinux)
            OS_PS1="%(?.%F{blue}.%F{red}) $(printf '\Uf303 ')%f"
            ;;
          debian)
            OS_PLUGINS+=(debian)
            ;;
          *)
            ;;
          esac
        ;;
      esac

      if systemctl --version &> /dev/null
      then
        OS_PLUGINS+=(systemd)
      fi
     ;;

   CYGWIN*|MINGW32*|MSYS*)
     #God help you...
     ;;

   *)
     #some fancy BSD?
     ;;
  esac
  if man --version &> /dev/null
  then
    OS_PLUGINS+=(man colored-man-pages)
  fi

  echo "OS_PLUGINS=($OS_PLUGINS)" > "$HOME/.zsh-os-plugins"
  if [[ ! -z "$OS_PS1" ]]
  then
    echo "PS1=\"$OS_PS1\"" >> "$HOME/.zsh-os-plugins"
    _PS1="$OS_PS1"
  fi
else
  source "$HOME/.zsh-os-plugins"
  _PS1="$PS1" # Workaround oh-my-zsh overriding my PS1
  # To Do:
  #     * Find out where my PS1 is being overriden and fix this upstream
  if [ -z "$OS_PLUGINS" ]
  then
    OS_PLUGINS=()
  fi
fi

# Additional plugins to be loaded
EXTRA_PLUGINS=(fzf direnv exa xdg-mime)

unalias tmux &> /dev/null

if [[ "$TMUX" ]]
then
  export FZF_TMUX=1
  export DISABLE_AUTO_TITLE=true # https://github.com/tmuxinator/tmuxinator#window-names-are-not-displaying-properly
  EXTRA_PLUGINS=(tmux tmuxinator vi-mode $EXTRA_PLUGINS)
else
  # Tmux crashes when running neovim under suckless terminal (st)
  # A workaround is to set the TERM to xterm or xterm-256color
  tmux() {
    [[ "$TERM" == "st" ]] && local TERM="xterm"
    [[ "$TERM" == "st-256color" ]] && local TERM="xterm-256color"

    env TERM="$TERM" tmux $@
  }
fi

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

if [[ -z "$plugins" ]]
then
  if [[ -z "$EXTRA_PLUGINS" ]]
  then
    EXTRA_PLUGINS=()
  fi
  common_plugins=(common-aliases command-not-found gitfast $([[ "${commands[fuck]}" ]] && printf "thefuck"))
  docker_plugins=(docker docker-compose)
  plugins=($OS_PLUGINS $common_plugins $EXTRA_PLUGINS)
  old_plugins=($plugins)
fi

source "$ZSH/oh-my-zsh.sh"

resetplugins() {
  unset plugins
  source "$XDG_CONFIG_HOME/zsh/.zshrc"
}

addplugin() {
  for plugin in $@
  do
    # Sanity check - Don't `source .zshrc` if we don't have to
    if [[ $plugins == *"$plugin"* ]]
    then
      if [ -z "$ADDED_PLUGINS" ]
      then
        ADDED_PLUGINS=0
      fi
      continue
    fi

    if typeset -p "${plugin}_plugins" > /dev/null 2>&1; then
      plugin+="_plugins"
      plugins+=${(P)plugin}
    else
      plugins+=$plugin
    fi

    # Work-arounds for plugins where load-order matters!
    if [[ "${plugins[(r)vi-mode]}" == "vi-mode" ]]
    then
      _plugin=vi-mode
      plugins=("${(@)plugins:#$_plugin}")
      plugins=($_plugin $plugins)
    fi

    ADDED_PLUGINS=1
  done

  if [[ "$ADDED_PLUGINS" -eq 1 ]]
  then
    source "$XDG_CONFIG_HOME/zsh/.zshrc"
  fi
}

_addplugin() {
  local PATH="/sbin:/bin:/usr/local/sbin:/usr/local/bin:/usr/bin"
  reply=( $(find -L "$ZSH/plugins" "$ZSH/custom/plugins" -type d -maxdepth 1 -execdir bash -c 'echo {} | cut -c 3-' \; | tail -n +2) )
}

compctl -K _addplugin addplugin

removeplugin() {
  for plugin in $@
  do
    plugins=("${(@)plugins:#$plugin}")
  done

  source "$XDG_CONFIG_HOME/zsh/.zshrc"
}

_removeplugin() {
  reply=($plugins)
}

compctl -K _removeplugin removeplugin

zshtheme() {
  if [ -z "$1" ]
  then
    echo "Usage: zshtheme THEME"
  else
    ZSH_THEME="$1" exec zsh
  fi
}

_zshtheme() {
  _themes=(random)
  reply=( $_themes $(ls -f "$ZSH/themes" | sed 's/.zsh-theme//g' | tail -n +3) )
}

compctl -K _zshtheme zshtheme

vim-colorscheme() {
  if [[ "$1" != "" ]]
  then
    export VIM_COLOR_SCHEME="$1"
  fi
}

_vim-colorscheme() {
  local PATH="/sbin:/bin:/usr/local/sbin:/usr/local/bin:/usr/bin"
  reply=( $(find "$HOME/.vim"/ -type d -name "colors" -execdir bash -c 'ls -f {} | grep -E ".*.vim" | rev | cut -c5- | rev' \;) )
}

compctl -K _vim-colorscheme vim-colorscheme

vim-background() {
  if [[ "$1" != "" ]]
  then
    export LC_TERM_PROFILE="$1"
  else
    export LC_TERM_PROFILE=light
  fi
}

_vim-background() {
  reply=(light dark)
}

compctl -K _vim-background vim-background

bootiso() {
  #qemu-system-x86_64 -enable-kvm -cpu host -vga virtio -m 8G -cdrom "$1"
  qemu-system-x86_64 -enable-kvm -cpu host -vga virtio -sdl -m 8G -cdrom "$1"
}

_bootiso() { _arguments '1: : _files -g "*.(#i)iso"' }
compdef {_,}bootiso

# Yarn, my $HOME is not your personal dumping ground for auto-generated files
# See: $XDG_CACHE_HOME or $XDG_CONFIG_HOME instead.
[ -n "${commands[yarn]}" ] && {
  yarn() {
    ${commands[yarn]} $@
    RET=$?
    [ -f "$HOME/.yarnrc" ] && rm -rf "$HOME"/.yarnrc
    return "$RET"
  }
}

# Helper function for running GUI applications
# with XWayland in a Wayland environment
x11-run() {
  GDK_BACKEND=x11
  QT_QPA_PLATFORM=xcb
  env "$@"
}

_x11-run() { reply=($(basename -a $commands)) }
compctl -K _x11-run x11-run

# User configuration
# You may need to manually set your language environment
export LANG=en_GB.UTF-8
export LC_CTYPE="$LANG"
export LC_COLLATE="$LANG"

# We may not want to look for a display e.g to save on performance when
# sourcing this profile or on a server.
[[ -z "$FIND_DISPLAY" ]] && FIND_DISPLAY=1
if [[ -z "$DISPLAY" && "$FIND_DISPLAY" -eq 1 ]]
then
  for display in {0..10}
  do
    export DISPLAY=:$display
    CAN_OPEN_DISPLAY=$(xrandr 2>/dev/stdout | grep "Can't open display" &> /dev/null 2>&1)
    if [[ "$?" -eq 1 ]]
    then
      # We've found a display
      break
    fi
    if [[ "$?" -eq 0 ]]
    then
      unset DISPLAY
    fi
  done
fi

if [[ -z "$LC_TERM_PROFILE" ]]
then
  export LC_TERM_PROFILE=dark
fi

if [[ -z "$VIM_COLOR_SCHEME" ]]
then
  if grep "wal/colors.Xresources" ~/.Xresources | grep -v -q '!'
  then
    export VIM_COLOR_SCHEME=wal
  fi
fi

export VIM_COLOR_SCHEME=nord

for f in cls clea cleat clearr
do
  alias $f="clear"
done

alias dropcaches='echo 3 | sudo tee /proc/sys/vm/drop_caches'
alias sytemctl='systemctl'
alias VboxManage="VBoxManage"

get_font() {
  font="$(pstree -p | grep $$ | grep -ohE 'st\([0-9]+\)' | tail -1 | grep -ohE '[0-9]+' | xargs -I '{}' ps {} | tail -1 | awk '{for(i=5;i<=NF;++i)print $i}' | xargs | grep -E '\-f .*' | awk -F '-f' '{print $2}' | awk -F '-' '{print $1}' | sed 's/:.*//g')"
  [ -z "$font" ] && font="$(grep -E '^static char \*font =.*' /etc/portage/savedconfig/x11-terms/st-0.8 | awk -F '"' '{print $2}' | sed 's/:.*//g')"

  printf "%s\\n" "$font"
}

alias neofetch='term_font="$(get_font)" neofetch --image ascii --disable title'
alias volup="amixer -q -D pulse sset Master 5%+"
alias voldown="amixer -q -D pulse sset Master 5%-"

jenkins-cli() {
  if [ -z "$JENKINS_URL" ]
  then
    export JENKINS_URL="http://192.168.0.11:8080"
  fi

  if [ -z "$JENKINS_BIN" ]
  then
    JENKINS_BIN="$HOME/Downloads/jenkins-cli.jar"
  fi

  if [ ! -f "$JENKINS_BIN" ]
  then
    curl -o "$JENKINS_BIN" -L "$JENKINS_URL/jnlpJars/jenkins-cli.jar"
  fi

  # Set pass to 'ssh user@somemachine pass' to use over ssh
  if [ -z "$pass" ]
  then
    export pass=pass
  fi

  java -jar ~/Downloads/jenkins-cli.jar -http -auth "$(sh -c '$pass 192.168.0.11:8080/jenkins')" "$@"
}

_readmd() {
  if [ -z "$PAGER" ]
  then
    PAGER=less
  fi

  if [[ "${commands[php]}" ]]
  then
    remove_html_attrs="php -r '\$handle = fopen(\"php://stdin\", \"r\");\$count = 0;while(!feof(\$handle)){\$buffer = fgets(\$handle);echo html_entity_decode(\$buffer);}fclose(\$handle);'"
  else
    if [[ "${commands[perl]}" ]]
    then
      remove_html_attrs="perl -pe 's/&[a-zA-Z].*?[a-zA-Z];//g'"
    else
      remove_html_attrs="cat"
    fi
  fi
  pandoc "$@" | eval "$remove_html_attrs" | html2text -style pretty | $PAGER
}

readmd() {
  for f in "$@"
  do
    if test -d "$f"
    then
      for f in "$f"/*.md
      do
        _readmd "$f"
      done
    else
      _readmd "$f"
    fi
  done
}

_readmd-w3m() {
  pandoc "$@" -t html | w3m -T text/html
}
readmd-w3m() {
  for f in "$@"
  do
    if test -d "$f"
    then
      for f in "$f"/*.md
      do
        _readmd-w3m "$f"
      done
    else
      _readmd-w3m "$f"
    fi
  done
}

diff() {
  command -v colordiff &> /dev/null && colordiff "$@" || env diff "$@"
}

alias webtorrent="webtorrent -o \"$HOME/Downloads\""
alias kitematic="gksudo kitematic"
alias powershell="powershell -NoLogo"
alias update="sudo pacman -Su && sudo pacman -Syu && pacaur -Su"
alias fuckingwindows="find . -type f -execdir dos2unix {} \;"

code() {
  (env code $@ &> /dev/null 2>&1 &)
}

gedit() {
  (env gedit $@ &> /dev/null 2>&1 &)
}

unalias vim &> /dev/null
unalias vi &> /dev/null
if builtin command -v nvim &> /dev/null
then
  vim() {
    env nvim $@
  }

  vi() {
    env nvim $@
  }

  export MANPAGER="nvim -c 'set ft=man' -"
  [ -f "$HOME/.bin/vimpager" ] && export PAGER=$HOME/.bin/vimpager
fi

alias pager="$PAGER"
export BAT_PAGER="less -RF"

# escapes the output from `mpc -f '%file%'` using `printf` and cd's
# to the directory the file is in (assuming XDG_MUSIC_DIR points to mpd's library)
mpcd() {
  eval "$(mpc -f '%file%' 2>/dev/null | head -1 | xargs -r -I'{}' dirname {} | xargs -r -I'{}' printf "cd %q/%q" "$XDG_MUSIC_DIR" {})"
}

# ipinfo's geoip database is more accurate than wttr.in's in my experience
# YMMV
weather() {
  if [ "$#" -eq 0 ]
  then
    curl -s https://wttr.in/`curl -s https://ipinfo.io 2> /dev/null | jq -r ".city" 2> /dev/null | xargs -r -I'{}' printf "%q" {}`
  else
    curl -s https://wttr.in/\~$@
  fi
}

docker() {
  sudo -E env docker "$@"
}

docker-compose() {
  sudo -E env docker-compose "$@"
}

calc() {
  bc <<< "$@"
}

#https://superuser.com/questions/957913/how-to-fix-and-recover-a-corrupt-history-file-in-zsh#957924
fix_zsh_history() {
  mv .zsh_history .zsh_history_bad
  strings .zsh_history_bad > .zsh_history
  fc -R .zsh_history
}

dotnet() {
  if [[ "$1" == "new" ]]
  then
    # `dotnet new` initialises a project with files in CRLF format - Let's fix that with dos2unix...
    env dotnet "$@" && dos2unix ./* &> /dev/null 2>&1
  else
    env dotnet "$@"
  fi
}

# This abomination (there really is no other word) checks for updates to custom vagrant boxes
# using the Last-Modified HTTP header and comparing that against a known folder in $HOME/.vagrant.d/boxes/MY_BOX/
vagrant box custom-update() {

if [[ "$@" != "box custom-update" ]]
then
  env vagrant "$@"
  return
fi

UPDATE_NEEDED=$(xargs -0 php -r << EOF
if (!file_exists('Vagrantfile')) {
  exit(1);
} else {
  \$box = \`grep -E 'config.vm.box = ".*"' Vagrantfile | sed 's/.*"http/http/g' | rev | cut -c 2- | rev | xargs printf \`;
}

\$normalised_box = \`sed 's/\//-VAGRANTSLASH-/g' <<< "\$box" | xargs printf\`;

\$curl = curl_init(\$box);

//don't fetch the actual page, you only want headers
curl_setopt(\$curl, CURLOPT_NOBODY, true);

//stop it from outputting stuff to stdout
curl_setopt(\$curl, CURLOPT_RETURNTRANSFER, true);

// attempt to retrieve the modification date
curl_setopt(\$curl, CURLOPT_FILETIME, true);

\$result = curl_exec(\$curl);

if (\$result === false) {
    die (curl_error(\$curl));
}

\$remote_timestamp = curl_getinfo(\$curl, CURLINFO_FILETIME);
if (\$remote_timestamp == -1) {
  exit(1);
}

\$HOME = getenv('HOME');
if (!isset(\$HOME)) {
  exit(1);
}

//\$timestamp = filemtime(\`ls -dh \$HOME/.vagrant.d/boxes/\$normalised_box/* | head -n1\`);
\$timestamp = \`ls -dh \$HOME/.vagrant.d/boxes/\$normalised_box/* | head -n1 | xargs printf\`;
if (file_exists(\$timestamp)) {
  \$timestamp = filemtime(\$timestamp);
  if (\$remote_timestamp > \$timestamp) {
    echo "{\"UPDATE_NEEDED\": \"" . \$normalised_box . "\", \"box\": \"" . \$box . "\"}";
  } else {
    echo "OK";
  }
}
EOF
)

if [[ "$UPDATE_NEEDED" == "{\"UPDATE_NEEDED\":"* ]]
then
  normalised_box="$(echo "$UPDATE_NEEDED" | grep -oE '{\"UPDATE_NEEDED\": \".*\",' | sed 's/.*: \"//g' | rev | cut -c 3- | rev)"
  box="$(echo "$UPDATE_NEEDED" | grep -oE '\"box\": \".*\"' | sed 's/\"box\":.* \"//g' | rev | cut -c 2- | rev)"
  echo "An updated box for $box is available!"
  printf "Do you want to update (y/n): "
  while true
  do
  read yn
  if [[ "$yn" == "Y" || $yn == "y" || "$yn" == "N" || "$yn" == "n" ]]
  then
    if [[ "$yn" == "N" || "$yn" == "n" ]]
    then
      break;
    fi
    env vagrant box add "$box" --name "$normalised_box" --force; break;
  else
    echo "Please answer y or n";
  fi
  done
else
  echo "Box is already up to date"
fi
}

# oh my zsh up arrow completion - https://github.com/robbyrussell/oh-my-zsh/issues/1720#issuecomment-286366959
# Fix vi-mode up / down completion
if [[ "${plugins[(r)vi-mode]}" == "vi-mode" ]]
then
  # start typing + [Up-Arrow] - fuzzy find history forward
  if [[ "${terminfo[kcuu1]}" != "" ]]; then
    autoload -U up-line-or-beginning-search
    zle -N up-line-or-beginning-search
    bindkey "${terminfo[kcuu1]}" up-line-or-beginning-search
  fi
  # start typing + [Down-Arrow] - fuzzy find history backward
  if [[ "${terminfo[kcud1]}" != "" ]]; then
    autoload -U down-line-or-beginning-search
    zle -N down-line-or-beginning-search
    bindkey "${terminfo[kcud1]}" down-line-or-beginning-search
  fi

  if [[ "$RPS1" ]]
  then
    if [[ "$ZSH_THEME" == "refined" ]]
    then
      MODE_INDICATOR="%{$fg_bold[red]%}<%{$fg[red]%}<<%{$reset_color%}"
      RPS1='$(vi_mode_prompt_info)'
    fi
  fi
fi

[ -f "$HOME/.aliases" ] && . "$HOME/.aliases"

[ -f "$HOME/.travis/travis.sh" ] && source "$HOME/.travis/travis.sh" || true

[[ -z "$_PS1" ]] && _PS1="$PS1"
[[ "$ZSH_THEME" == "refined" ]] && export PS1="$_PS1"
unset _PS1

[ -n "$RANGER_LEVEL" ] && PS1="$PS1"'(in ranger) ' || true
