# .Dotfiles

GNU Stow manages all of the direcories. The dotfiles in the root directory are manually symlinked (see dotfiles_dotfiles in the Makefile)

## Dependencies

### Required:
* [GNU Make](https://www.gnu.org/software/make/)
* [GNU Parallel](https://www.gnu.org/software/parallel/) (potentially replaceable with `xargs`?)
* [GNU Stow](https://www.gnu.org/software/stow/)

### Optional:
* [Git](https://git-scm.com/) (for cloning various repo's I'm interested in - Such as in my [git_repos.sh](https://gitlab.com/ahrs/dotfiles/blob/master/git_repos.sh) script)
* [Curl](https://curl.haxx.se/) - Used for remote code execution to install [n-install](https://github.com/mklement0/n-install), [rustup](https://rustup.rs/) and [dein](https://github.com/Shougo/dein.vim)
* [GNU Coreutils](https://www.gnu.org/software/coreutils/coreutils.html) (This should be optional now, if it's not, that's a bug)
* [Python](https://www.python.org/) and [GCC/G++](https://www.gnu.org/software/gcc/) - Used by [node-gyp](https://github.com/nodejs/node-gyp) to install native nodejs modules

## Testing

For testing purposes, the dotfiles can be bootstrapped in a Docker container. To bootstrap a [Suicide Linux](https://github.com/tiagoad/suicide-linux) container run `make DEBUG=1 docker_suicide_linux`. A successful bootstrap should exit with a zero exit code.

Currently the dotfiles can be bootstrapped in the following environments:

  * Alpine Linux (only dotfiles, n-install and rustup don't work on Alpine, possibly due to its use of musl?)
  * Arch Linux
  * CentOS
  * Debian
  * Fedora
  * FreeBSD
  * Gentoo
  * macOS
  * OpenBSD (Requires other dependencies to be pre-installed - To Do: Fix this)
  * OpenSUSE
  * Suicide Linux
  * Ubuntu
  * Void Linux
