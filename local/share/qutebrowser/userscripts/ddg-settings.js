#!/usr/bin/env node
const qutejs = require('qutejs');

const wal = require(process.env.HOME + '/.cache/wal/colors.json')

// based on - https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb#5624139
const hexToRgb = hex => {
  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
  const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, (m, r, g, b) => {
    return r + r + g + g + b + b;
  });

  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
};

const foreground = wal.special.foreground;
const background = wal.special.background;
const cursor = wal.special.cursor;

const colors = {
  "black":    wal.colors.color0,
  "red":      wal.colors.color1,
  "green":    wal.colors.color2,
  "yellow":   wal.colors.color3,
  "blue":     wal.colors.color4,
  "magenta":  wal.colors.color5,
  "cyan":     wal.colors.color6,
  "white":    wal.colors.color7,
  "grey":     wal.colors.color8,
  "color0":   wal.colors.color0,
  "color1":   wal.colors.color1,
  "color2":   wal.colors.color2,
  "color3":   wal.colors.color3,
  "color4":   wal.colors.color4,
  "color5":   wal.colors.color5,
  "color6":   wal.colors.color6,
  "color7":   wal.colors.color7,
  "color8":   wal.colors.color8,
  "color9":   wal.colors.color9,
  "color10":  wal.colors.color10,
  "color11":  wal.colors.color11,
  "color12":  wal.colors.color12,
  "color13":  wal.colors.color13,
  "color14":  wal.colors.color14,
  "color15":  wal.colors.color15,
}

// Deliberatly pollute the global namespace so we can
// use "red" as a shortcut to colors.red
Object.keys(colors).forEach(k => {
  global[k] = colors[k];
});

qutejs.open(['-t', 'https://duckduckgo.com/settings#appearance']);

setTimeout(() => {
  qutejs.jseval([
    `
    (function() {
      document.cookie = '7=${background}'; /* background color */
      document.cookie = 'j=${background}'; /* header color */
      document.cookie = '9=${blue}'; /* result title color */
      document.cookie = 'aa=${magenta}'; /* result visited color */
      document.cookie = '8=${foreground}'; /* result description color */
      document.cookie = 'x=${grey}'; /* result url color */
      document.cookie = 'y=${yellow}'; /* result highlight color */
      document.cookie = 't=monospace'; /* font */
      document.cookie = 'a=monospace'; /* result title font */
      
      location.reload();
    })();
    `.split('\n').join('')
  ]);
  setTimeout(() => {
    qutejs.jseval([
    `
    (function() {
      window.location.href = document.getElementsByClassName('js-set-bookmarklet-url')[0].value.replace('start.duckduckgo.com','duckduckgo.com').replace('duckduckgo.com','start.duckduckgo.com');
    })();
     `.split('\n').join('')
    ]);
    setTimeout(() => {
      qutejs.set(['url.default_page', '{url}']);
    }, 2000);
  }, 4000);
}, 4000);
