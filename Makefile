DEBUG?=0
CI?=
N_INSTALL_HASH?=fd398d200e797a0b9b17b762de83f6be35f5545b1ca4ed858a9c47ef5fcd7ac4
RUSTUP_HASH?=2dceccb17a760ff4845d945689ab5d799083406a76831eb7c83d26705f688495
DEIN_HASH?=991b852cdb74af23396ddcc6933d0ef682dee1e379dab0e56a08e6b4f4f931c
DOTNET_HASH?=a77dc7db6ef2f1bc9544d4144fd154dd54c3a6f133ec856f9a6dd32eaca6fb0a
DOCKER?=docker
SUDO?=sudo
BASH?=bash
MKDIR?=mkdir
FIND?=find
PARALLEL?=parallel
STOW?=stow
STOW_ARGS?=
NODE_ARCH?=x64
PULL?=0
TMPDIR?=/tmp
XDG_CONFIG_HOME?=$(HOME)/.config
XDG_DATA_HOME?=$(HOME)/.local/share

ifeq ($(DEBUG), 1)
    STOW_VERBOSE=-v
endif

all: will-cite-parallel dotfiles nodejs rustup dein

will-cite-parallel:
	$(MKDIR) -p "$(XDG_CONFIG_HOME)/parallel"
	touch "$(XDG_CONFIG_HOME)/parallel/will-cite"

git_repos:
	./git_repos.sh

oh-my-zsh: git_repos
	 STOW_DIR="$(PWD)/bin" $(STOW) $(STOW_ARGS) $(STOW_VERBOSE) zsh-plugins -t "$(HOME)/.bin/.repos/github.com/robbyrussell/oh-my-zsh/custom/plugins"

dotfiles_bin:
	$(MKDIR) -p "$(HOME)/.bin"
	$(STOW) $(STOW_ARGS) $(STOW_VERBOSE) bin -t "$(HOME)/.bin"

dotfiles_config:
	$(MKDIR) -p "$(XDG_CONFIG_HOME)"
	$(STOW) $(STOW_ARGS) $(STOW_VERBOSE) config -t "$(XDG_CONFIG_HOME)"

dotfiles_local:
	$(MKDIR) -p "$(HOME)/.local"
	$(STOW) $(STOW_ARGS) $(STOW_VERBOSE) local -t "$(HOME)/.local"

dotfiles_dotfolders:
	@$(FIND) . \
		-maxdepth 1 \
		-type d | grep -v -E '^(.$$|./bin$$|./.git$$|./config$$|./local$$)' | PARALLEL_HOME="$(HOME)/.config/parallel" $(PARALLEL) 'mkdir -p "$(HOME)/.$$(basename {})"'\;$(STOW) $(STOW_ARGS) $(STOW_VERBOSE) '$$(basename {})' -t '$(HOME)/.$$(basename {})'

dotfiles_dotfiles:
	@$(FIND) -L . \
		-maxdepth 1 \
		-type f \
		| grep -v -E '^(./tags$$|./LICENSE|./.*.bak$$|./.envrc$$|./.gitmodules|./.gitlab-ci.yml$$|./.gitignore$$|./bootstrap.sh$$|./git_repos.sh$$|./get-winblows-path.ps1$$|./Makefile$$|./Xresources.desktop$$|./Xresources.laptop$$|./README.md$$)' | sed "s/^.\//$$(basename "$(PWD)")\//g" | PARALLEL_HOME="$(XDG_CONFIG_HOME)/parallel" $(PARALLEL) 'ln --help 2>&1 | grep -i -q verbose && HAS_VERBOSE="-v" || HAS_VERBOSE="";[ "$$HAS_VERBOSE" = "" ] && printf "\'\''$(HOME)/.$$(basename {})\'\'' -> \'\''{}\'\''\\n";ln $${HAS_VERBOSE} -fs "{}" "$(HOME)/.$$(basename {})"'

dotfiles: will-cite-parallel dotfiles_bin dotfiles_config dotfiles_local dotfiles_dotfiles dotfiles_dotfolders

N_PREFIX?=$(XDG_DATA_HOME)/n
NPM?=$(N_PREFIX)/bin/npm
nodejs:
	mkdir -p "$(XDG_DATA_HOME)"
	#https://github.com/mklement0/n-install
	if [ ! -d "$(XDG_DATA_HOME)/n" ] ; \
	then \
	./bin/curlbash -e $(N_INSTALL_HASH) -c -f https://git.io/n-install && sed 's/\${\#existingExes\[@\]} > 0/\${\#existingExes\[@\]} < -1/g' $(TMPDIR)/7cc6426fbb67f615ad774e94624c911f2d97b5eeded704905e5d3a95db62e18a | env N_PREFIX=$(N_PREFIX) $(BASH) -s -- -n -q -y && \
		$(BASH) -c 'SHELL=$$(command -v bash) N_PREFIX=$(N_PREFIX) $(N_PREFIX)/bin/n --arch $(NODE_ARCH) lts' && \
		$(BASH) -c 'SHELL=$$(command -v bash) N_PREFIX=$(N_PREFIX) $(N_PREFIX)/bin/n --arch $(NODE_ARCH) latest' && \
		env N_PREFIX=$(N_PREFIX) npm_config_prefix=$(N_PREFIX) $(NPM) install -g yarn; \
		command -v nvim > /dev/null 2>&1 && $(N_PREFIX)/bin/yarn global add neovim; \
	fi

rustup:
	#https://github.com/rust-lang-nursery/rustup.rs
	./bin/curlbash -e $(RUSTUP_HASH) -c -f https://sh.rustup.rs && sh $(TMPDIR)/8e22a135acc62b7185a2ed9a117d36803e5cf1e60f1ceb5e00d729e0d25befd7 -y --no-modify-path

DOTNET_INSTALL_DIR?="$(XDG_DATA_HOME)/dotnet"
DOTNET_CHANNEL?=Current
DOTNET_EXTRA_ARGS?=
dotnet:
	# https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-install-script
	./bin/curlbash -e $(DOTNET_HASH) -c -f https://dot.net/v1/dotnet-install.sh && bash $(TMPDIR)/2b8f80a2b1877dd06bdf6e4efcac098f6311273a7637a39cbf5f2e90b8892a0e -c $(DOTNET_CHANNEL) --install-dir $(DOTNET_INSTALL_DIR) $(DOTNET_EXTRA_ARGS)

dein:
	./bin/curlbash -e 943163d3660423febad96fe91371807763679a683947b44b63254dc2d555094c -c -f https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh && sh $(TMPDIR)/ca91cea7e28b97ed56b23e4c908ef905c6c47d7be6bfeb35334ccae6cc1e7830 "$(HOME)/.vim"

	# Vim has issues quiting, if an error occurs,
	# this "Presses Enter" (repeatedly) to continue
	if command -v vim && [ "$$(vim --version | head -1 | awk '{print $$1}' | xargs)" != "NVIM" ]; \
	then \
	   yes $$'\n' | vim +":call dein#install()" +qall < /dev/tty; \
	fi

	# Neovim is much more sensible and actually has its own "headless" mode
	if command -v nvim ; \
	then \
		nvim +":call dein#install()" +qall --headless; \
	fi

	@printf $$'\n'

docker_all:
	$(MAKE) $$(grep -ohE 'docker_.*:' Makefile | grep -v docker_test | grep -v docker_all | rev | sed 's/^.*://g' | rev | sort)

docker_test:
	if [ "$(PULL)" -eq 1 ]; then \
		$(SUDO) $(DOCKER) pull $(IMAGE); \
	fi

	$(SUDO) $(DOCKER) run -it --rm -v $(PWD):$(PWD) $(VOLUMES) -w $(PWD) $(IMAGE) sh -c "$(DOCKER_RUN_CMD) CI=$(CI) DEBUG=$(DEBUG) OS="$(OS)" OS_LIKE="$(OS_LIKE)" ./bootstrap.sh $(BOOTSTRAP_ARGS)"

docker_alpine:
	$(MAKE) IMAGE=alpine BOOTSTRAP_ARGS="dotfiles" docker_test

docker_arch:
	$(MAKE) IMAGE=archlinux/base DOCKER_RUN_CMD="yes | pacman -Syu --needed --noconfirm base;" docker_test

docker_centos:
	$(MAKE) IMAGE=centos docker_test

docker_debian:
	$(MAKE) IMAGE=debian docker_test

docker_fedora:
	$(MAKE) IMAGE=fedora docker_test

docker_gentoo:
	$(MAKE) IMAGE=gentoo/stage3-amd64 VOLUMES=-v\ /usr/portage:/usr/portage docker_test

docker_opensuse: docker_opensuse_leap docker_opensuse_tumbleweed

docker_opensuse_leap:
	$(MAKE) IMAGE=opensuse/leap docker_test

docker_opensuse_tumbleweed:
	$(MAKE) IMAGE=opensuse/tumbleweed docker_test

docker_suicide_linux:
	# Mount our volumes read-only because I really don't trust myself ;)
	$(SUDO) $(DOCKER) run -it --rm -v $(PWD):$(PWD):ro -w $(PWD) tiagoad/suicide-linux -c "DEBUG=$(DEBUG) CI=$(CI) ./bootstrap.sh"

docker_ubuntu:
	$(MAKE) IMAGE=ubuntu docker_test

docker_void:
	$(MAKE) IMAGE=voidlinux/voidlinux docker_test
