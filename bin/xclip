#!/bin/sh

# shellcheck disable=SC2086
# Disable shellcheck warning about word-splitting,
# this is intentional. Without wordsplitting
# "-arg1 -arg2" is treated as one argument

if [ -z "$WAYLAND_DISPLAY" ]
then
  # This script should be first in my $PATH.
  # By telling env to use a default environment with -i
  # it should ignore my custom $PATH and fallback
  # to the real xclip when ran under an X11 WM.
  # XAUTHORITY and DISPLAY are needed environment
  # variables. Not sure about LANG/LOCALE but added
  # it anyway for completeness.
  # Note: This may fail under distros that don't have the
  # $PATH to xclip in the default $PATH. I've seen some
  # distros where `env -i /bin/sh -c 'echo $PATH'` does
  # not contain `/usr/bin` ¯\_(ツ)_/¯
  exec /usr/bin/env -i \
    LANG="$LANG" \
    LC_CTYPE="$LC_CTYPE" \
    LC_NUMERIC="$LC_NUMERIC" \
    LC_TIME="$LC_TIME" \
    LC_COLLATE="$LC_COLLATE" \
    LC_MONETARY="$LC_MONETARY" \
    LC_MESSAGES="$LC_MESSAGES" \
    LC_PAPER="$LC_PAPER" \
    LC_NAME="$LC_NAME" \
    LC_ADDRESS="$LC_ADDRESS" \
    LC_TELEPHONE="$LC_TELEPHONE" \
    LC_MEASUREMENT="$LC_MEASUREMENT" \
    LC_IDENTIFICATION="$LC_IDENTIFICATION" \
    DISPLAY="$DISPLAY" \
    XAUTHORITY="$XAUTHORITY" xclip "$@"
fi

copy=1
paste=0
selection=clipboard
mimetype=""
file=""
args=""

while [ "$#" -gt 0 ]
do
  arg="$1"
  case "$arg" in
    -i|-in)
      copy=1
      paste=0
      ;;
    -o|-out)
      paste=1
      copy=0
      ;;
    -selection)
      shift 1
      selection="$1"
      ;;
    -t|--type)
      shift 1
      mimetype="$1"
      ;;
    -l|--loops)
      shift 1
      ;;
    -quiet)
      ;;
    -silent)
      ;;
    -verbose)
      ;;
    -rmlastnl)
      ;;
    -noutf8)
      ;;
    -target)
      ;;
    -h|--help)
      ;;
    *)
      if [ -f "$arg" ]
      then
        if [ -z "$file" ]
        then
          file="$arg"
        fi
      fi
      ;;
  esac
  shift 1
done

[ "$selection" = "primary" ] && args="${args} --primary"

if [ "$paste" -eq 1 ]
then
  exec wl-paste $args --no-newline
fi

if [ "$copy" -eq 1 ]
then
  if [ -n "$file" ]
  then
    [ -z "$mimetype" ] && mimetype="$(file --brief -i "$file")"
    # wl-copy doesn't fork properly when passed a file
    # so `nohup` is needed. Bug?
    nohup wl-copy -t "$mimetype" $args < "$file" > /dev/null 2>&1 & disown > /dev/null 2>&1 || true
  else
    # default mimetype is needed to prevent firefox from
    # mis-interpreting its contents and pasting random Unicode
    # characters. wl-copy is supposed to have some sort of
    # automagic way of interpreting the correct mimetype so
    # maybe this fails? Explicit is better than implicit
    # anyway, might as well assume we're dealing with plain
    # text unless told otherwise.
    mimetype="${mimetype:-text/plain}"
    exec wl-copy -t "$mimetype" $args
  fi
fi
