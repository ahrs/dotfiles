#!/usr/bin/env bash
WAL=/usr/bin/wal

if [ ! -f "$WAL" ]
then
  exit 1
fi

if command -v sudo &> /dev/null
then
  SUDO=sudo
fi

sudo() {
  if [ -z "$SUDO" ]
  then
    printf "Unable to escalate root privileges\\n"
    exit 1
  fi

  env $SUDO "$@"
}

[ -f /etc/os-release ] && . /etc/os-release

setup_in_progress() {
  printf "\\nSome previous setup stages are still in progress. To avoid creating lots of processes we won't run the setup this time.\\nRun \`wal --setup\` to manually run the setup\\n"
  exit 1
}

after_wal() {
  if [[ "$*" == *"-h"* || "$*" == *"--help"* ]]
  then
    return
  fi

  pkill dunst
  ln -rsf ~/.cache/wal/dunstrc ~/.config/dunstrc
  dunst -conf "$HOME/.config/dunstrc" &> /dev/null &
  disown
  if [ -f ~/.config/discord/0.0.4/modules/discord_desktop_core/Makefile ]
  then
    [[ "$*" == *"-l"* ]] && LIGHT_MODE="false" || LIGHT_MODE="true"
    pushd ~/.config/discord/0.0.4/modules/discord_desktop_core && sed -i "s/^\$darkMode: .*/\$darkMode: $LIGHT_MODE \\!default;/g" discord-custom.scss && make
    popd || true
  fi
  if [ -n "$NAME" ]
  then
    case "$NAME" in
      Gentoo)
        rm -rf /tmp/colors.sh
        cp ~/.cache/wal/colors.sh /tmp
        rm -rf /tmp/colors-wal-st.h
        cp ~/.cache/wal/colors-wal-st.h /tmp
        if [[ "$(ps aux | grep -i 'emerge -q st' | grep -v grep)" == "" ]]
        then
          sudo tmux new-session -s st -d /usr/bin/emerge -q st &> /dev/null &
          disown
        else
          setup_in_progress
        fi
        if [[ "$(ps aux | grep -i 'emerge -q adapta-gtk-theme-wal' | grep -v grep)" == "" ]]
        then
          sudo tmux new-session -s emerge-adapta-gtk-theme-wal -d /usr/bin/emerge -q adapta-gtk-theme-wal &> /dev/null &
          disown
        else
          setup_in_progress
        fi
        ;;
      *)
        ;;
    esac
  fi
}

if [[ -n "$1" ]] && [[ "$1" == "--setup" ]]
then
  after_wal
  exit $?
fi

$WAL "$@" && after_wal "$@"
