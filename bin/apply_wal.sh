#!/usr/bin/env bash

COLORS="$(xrdb -query | grep -E 'color[0-9]')"

cp ~/.tmux.conf.template ~/.tmux.conf.local

for color in $(grep -ohE '::color[0-9]::' ~/.tmux.conf.template | sort -u)
do
  color="$(sed 's/:://g' <<< $color)"
  _color="$(echo "$COLORS" | grep "$color:" | sed 's/.*://g' | sort -u | xargs)"
  sed -i "s/::$color::/\\$_color/g" ~/.tmux.conf.local
done
