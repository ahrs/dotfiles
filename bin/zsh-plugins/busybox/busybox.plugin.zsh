if ! builtin command -v busybox &> /dev/null
then
  return
fi

export fpath=($ZSH/custom/plugins/busybox/src $fpath)

compinit
