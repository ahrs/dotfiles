if ! builtin command -v fzf &> /dev/null
then
  [[ -f "$HOME/.fzf/bin/fzf" ]] && PATH="$PATH:$HOME/.fzf/bin" || return
  export PATH
fi

# Manual install in /usr/share/fzf
if [[ -d /usr/share/fzf/shell ]]
then
  [[ -f /usr/share/fzf/shell/completion.zsh ]] && . /usr/share/fzf/shell/completion.zsh
  [[ -f /usr/share/fzf/shell/key-bindings.zsh ]] && . /usr/share/fzf/shell/key-bindings.zsh
  return
fi

# Manual install in ~/.fzf (default if following - https://github.com/junegunn/fzf#using-git)
if [[ -d "$HOME/.fzf/shell" ]]
then
  [[ -f "$HOME/.fzf/shell/completion.zsh" ]] && . "$HOME/.fzf/shell/completion.zsh"
  [[ -f "$HOME/.fzf/shell/key-bindings.zsh" ]] && . "$HOME/.fzf/shell/key-bindings.zsh"
  return
fi

# Gentoo
[[ -f /usr/share/zsh/site-contrib/fzf.zsh ]] && . /usr/share/zsh/site-contrib/fzf.zsh && return

# Arch
if [[ -d /usr/share/fzf ]]
then
  [[ -f /usr/share/fzf/completion.zsh ]] && . /usr/share/fzf/completion.zsh
  [[ -f /usr/share/fzf/key-bindings.zsh ]] && . /usr/share/fzf/key-bindings.zsh
  return
fi

# Debian/Ubuntu

if [[ -f /usr/share/doc/fzf/examples/key-bindings.zsh ]]
then
  . /usr/share/doc/fzf/examples/key-bindings.zsh
fi
