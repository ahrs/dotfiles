if ! builtin command -v exa &> /dev/null
then
  return
fi

unalias ls &> /dev/null
unalias l &> /dev/null
unalias ll &> /dev/null
unalias la &> /dev/null
alias ls="exa"

alias la="exa -alh --git --icons"

alias ll="exa -l --git --icons"

alias l="exa -lFh --git --icons"
