# common-aliases uses things that are uncommon to OpenBSD - Let's fix that
source "$ZSH/plugins/common-aliases/common-aliases.plugin.zsh"
if ! grep '' <<< 'test' &> /dev/null
then
  unalias grep &> /dev/null
  unalias sgrep &> /dev/null
fi
