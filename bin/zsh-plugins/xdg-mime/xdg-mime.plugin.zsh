if ! builtin command -v xdg-mime &> /dev/null
then
  return
fi

export fpath=($ZSH/custom/plugins/xdg-mime/src $fpath)

compinit
