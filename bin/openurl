#!/usr/bin/env node
const { URL } = require('url');
const { execSync, spawn } = require('child_process');
const { readFileSync, writeFileSync, existsSync } = require('fs');

delete process.argv[0];
delete process.argv[1];

//process.env.GTK_THEME = 'Adapta';
process.env.MOZ_USE_XINPUT2=1;

try {
  // get the correct BROWSER in case we've changed it at runtime
  process.env.BROWSER = execSync(`sh -c '. ~/.bin/browser;printf '%s' "$BROWSER"'`);
} catch (err) {
  // whatever...
}

if (typeof process.argv[2] === 'undefined') {
  prog = process.env.BROWSER || 'chromium';
  args = [];
  if (Boolean(process.env.OPEN_PINNED_TABS) === true) {
    process.env.XDG_CONFIG_HOME = (!process.env.XDG_CONFIG_HOME) ? `${process.env.HOME}/.config` : process.env.XDG_CONFIG_HOME;
    let pinnedTabs = `${process.env.XDG_CONFIG_HOME}/pinned-tabs.json`;
    if (existsSync(pinnedTabs)) {
      try {
        pinnedTabs = JSON.parse(readFileSync(pinnedTabs));
        args.push(...pinnedTabs);
      } catch (err) {
        // nothing to see here...
      }
    }
  }
  spawn('/usr/bin/env', [prog, ...args], {
    detatched: true,
    stdio: 'ignore'
  }).unref();
}

var mimetype = null;
var desktopfile = null;

const getmime = function(url) {
  if (mimetype !== null) {
    return mimetype;
  }

  mimetype = execSync(`curl -s --head --write-out '%{content_type}' '${url.href}'`).toString().split('\n').pop();
  mimetype = (mimetype.length === 0) ? 'text/html' : mimetype;

  return mimetype;
};

const getdesktopfile = function() {
  if (desktopfile !== null) {
    return desktopfile;
  }

  desktopfile = execSync(`xdg-mime query default ${getmime().toString()}`).toString().trim();

  return desktopfile;
};

process.argv.forEach(url => {
  let prog = 'xdg-open';
  let args = [];
  let rundesktopfile = false;
  try {
    url = new URL(url);

    switch (url.hostname) {
      case 'www.reddit.com':
        url.hostname = 'old.reddit.com';
        break;
    }

    writeFileSync('/tmp/lasturl', url.href);

    // hack to write-out the JSON representation of the URL Object since url.toJSON() == url.toString()
    _x = {}
    for (prop in url) { _x[prop] = url[prop]; }
    if (_x.hasOwnProperty('toJSON')) { delete _x['toJSON']; }
    writeFileSync('/tmp/lasturl_json', JSON.stringify(_x));
    delete _x;

    //prog = (url.hostname.endsWith('reddit.com')) ? 'firefox' : prog;
    // Watch youtube videos in mpv
    if (
      (
        (url.hostname == 'www.youtube.com' || url.hostname == 'youtube.com') &&
         url.pathname == '/watch'
      ) || (
        (url.hostname == 'youtu.be' && url.pathname != '/')
      ) || (
        getmime(url).startsWith('video/')
      )
    ) {
      prog = 'mpv';
    } else if (getmime(url).startsWith('image/')) {
      if (getmime(url).toString().indexOf('svg') === -1) {
        prog = 'fehurl';
      } else {
        rundesktopfile = true;
      }
    }
    if (rundesktopfile) {
      if (getdesktopfile().endsWith('.desktop')) {
        prog = 'gtk-launch';
        args = [getdesktopfile(), '--', ...args];
      }
    }
  } catch (e) { /* error, what error? */ }

  // prevent recursion :)
  if (prog === 'xdg-open') {
    //if (getdesktopfile() === 'openurl.desktop' ||
    //    getmime().indexOf('text/plain') > -1
    //) {
      prog = process.env.BROWSER || 'firefox';
      prog = (prog === 'openurl') ? 'firefox' : prog;
    //}
  }

  spawn('/usr/bin/env', [prog, ...args, url.href||url||""], {
    detatched: true,
    stdio: 'ignore'
  }).unref();
});
