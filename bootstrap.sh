#!/bin/sh

# To Do:
#  * Install vim/neovim and dependencies

USER="$(id -u -n)"

export USER

unset HOME
HOME="${HOME:-~$USER}"
HOME="$(eval "echo $HOME")"

export HOME

XDG_DATA_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}"

export XDG_DATA_HOME

set -e

if [ -n "$DEBUG" ]
then
  [ "$DEBUG" -eq 1 ] && set -x
fi

POSIXLY_CORRECT=1; export POSIXLY_CORRECT

[ "$(id -u)" -eq 0 ] && [ -z "$SUDO" ] && SUDO="command"

# shellcheck disable=SC2120
mktemp() {
  # shellcheck disable=SC2068
  if ___the_tmp_file="$(command -- mktemp $@)"
  then
    printf "%s" "$___the_tmp_file"
    return 0
  fi
  for d in "$TMPDIR" "/tmp" "/dev/shm" "$PWD" "/"
  do
    [ -z "$d" ] && continue
    ___the_tmp_file="$d/dotfiles_$(date +%s)"
    touch "$___the_tmp_file" && {
      printf "%s" "$___the_tmp_file"
      return 0
    }
  done
  return 1
}

# shellcheck disable=SC2015
[ -e /dev/null ] && NULL=/dev/null || {
  # shellcheck disable=SC2119
  if ! NULL="$(mktemp)"
  then
    echo "Unable to create a temp file for NULL output"
    exit 1
  fi
}

sudo() {
  # shellcheck disable=SC2068
  /usr/bin/env sudo $@
}

[ -z "$SUDO" ] && [ "$(uname -s)" = "OpenBSD" ] && SUDO=doas
[ -z "$SUDO" ] && SUDO=sudo

# OpenSuse's docker container lacks a nobody user - Let's fix that!
if ! id nobody > "$NULL" 2>&1
then
  $SUDO groupadd -g 65534 nobody
  $SUDO useradd -s "$NULL" -M -u 65534 -g nobody nobody
fi

is_gnu_parallel() {
  parallel --help | grep -q -F 'GNU Parallel'
  return "$?"
}

pkgs=''

apt_install() {
  if ! command -v make > "$NULL" 2> "$NULL"
  then
    pkgs="$pkgs make"
  fi
  if ! command -v stow > "$NULL" 2> "$NULL"
  then
    pkgs="$pkgs stow"
  fi
  if ! command -v git > "$NULL" 2> "$NULL"
  then
    pkgs="$pkgs git"
  fi
  if ! command -v stow > "$NULL" 2> "$NULL"
  then
    pkgs="$pkgs curl"
  fi
  if ! command -v parallel > "$NULL" 2> "$NULL" || ! is_gnu_parallel
  then
    pkgs="$pkgs parallel"
  fi
  if ! command -v python2 > "$NULL" 2> "$NULL"
  then
    pkgs="$pkgs python"
  fi
  if ! command -v gcc > "$NULL" 2> "$NULL" || ! command -v g++ > "$NULL" 2> "$NULL"
  then
    pkgs="$pkgs build-essential"
  fi
  if ! command -v curl > "$NULL" 2> "$NULL"
  then
    pkgs="$pkgs curl"
  fi
  [ "$pkgs" != "" ] && $SUDO apt-get -qq update
  # shellcheck disable=SC2086
  [ "$pkgs" != "" ] && $SUDO apt-get -qq install -y $pkgs

  return 0
}

pacman_install() {
  if ! command -v make > "$NULL" 2> "$NULL"
  then
    pkgs="$pkgs make"
  fi
  if ! command -v stow > "$NULL" 2> "$NULL"
  then
    pkgs="$pkgs stow"
  fi
  if ! command -v curl > "$NULL" 2> "$NULL"
  then
    pkgs="$pkgs curl"
  fi
  if ! command -v git > "$NULL" 2> "$NULL"
  then
    pkgs="$pkgs git"
  fi
  if ! command -v parallel > "$NULL" 2> "$NULL" || ! is_gnu_parallel
  then
    pkgs="$pkgs parallel"
  fi
  if ! command -v sha256sum > "$NULL" 2> "$NULL"
  then
    pkgs="$pkgs coreutils"
  fi
  if ! command -v shasum > "$NULL" 2> "$NULL" && [ ! -f /usr/bin/core_perl/shasum ]
  then
    pkgs="$pkgs perl"
  fi
  # shellcheck disable=SC2086
  if [ "$pkgs" != "" ]
  then
    $SUDO pacman -Sy --noconfirm --needed --overwrite \\* $pkgs
  fi
}

case "$(uname -s)" in
  Darwin)
    if ! xcode-select -p > "$NULL" 2> "$NULL"
    then
      xcode-select --install
    fi
    if ! command -v brew
    then
      /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    fi
    if brew --version > "$NULL" 2> "$NULL"
    then
      if ! command -v gmake > "$NULL" 2> "$NULL"
      then
        pkgs="$pkgs make"
      fi
      if ! command -v stow > "$NULL" 2> "$NULL"
      then
        pkgs="$pkgs stow"
      fi
      if ! command -v curl > "$NULL" 2> "$NULL"
      then
        pkgs="$pkgs curl"
      fi
      if ! command -v git > "$NULL" 2> "$NULL"
      then
        pkgs="$pkgs git"
      fi
      if ! command -v parallel > "$NULL" 2> "$NULL" || ! is_gnu_parallel
      then
        pkgs="$pkgs parallel"
      fi
      # shellcheck disable=SC2086
      [ "$pkgs" != "" ] && brew install -y $pkgs
    fi
    ;;
  Linux)
    [ -z "$OS" ] && OS="$(grep -E '^ID=' /etc/os-release | sed -e 's/^.*=//g' -e 's/\"//g')"
    case "$OS" in
      ubuntu|debian)
        apt_install
        ;;
      alpine)
        if ! command -v make > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs make"
        fi
        if ! command -v stow > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs stow"
        fi
        if ! command -v curl > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs curl"
        fi
        if ! command -v git > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs git"
        fi
        if ! command -v parallel > "$NULL" 2> "$NULL" || ! is_gnu_parallel
        then
          pkgs="$pkgs parallel"
        fi
        if ! command -v bash > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs bash"
        fi
        # shellcheck disable=SC2086
        [ "$pkgs" != "" ] && $SUDO apk add --no-cache $pkgs
        ;;
      fedora|centos|rhel)
        cmd="dnf"
        if ! command -v dnf > "$NULL" 2> "$NULL"
        then
          cmd="yum"
        fi
        if [ "$OS" = "centos" ]
        then
          command -v yum > "$NULL" 2> "$NULL" && $SUDO $cmd install -y epel-release
        fi
        if ! command -v make > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs make"
        fi
        if ! command -v stow > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs stow"
        fi
        if ! command -v curl > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs curl"
        fi
        if ! command -v git > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs git"
        fi
        if ! command -v which > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs which"
        fi
        if ! command -v parallel > "$NULL" 2> "$NULL" || ! is_gnu_parallel
        then
          pkgs="$pkgs parallel"
        fi
        if ! command -v ps > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs procps-ng"
        fi
        if ! command -v find > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs findutils"
        fi
        if ! command -v fuck > "$NULL" 2> "$NULL"
        then
          if $cmd whatprovides fuck > "$NULL" 2> "$NULL"
          then
            pkgs="$pkgs thefuck"
          fi
        fi
        if ! command -v shasum > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs perl perl-Digest"
        fi
        # shellcheck disable=SC2086
        [ "$pkgs" != "" ] && $SUDO $cmd install -y $pkgs
        unset cmd
        ;;
      gentoo)
        if ! command -v make > "$NULL" 2> "$NULL"
        then
          # interestingly make needs to be built with make so this
          # will probably fail if make isn't installed for some reason
          pkgs="$pkgs sys-devel/make"
        fi
        if ! command -v stow > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs app-admin/stow"
        fi
        if ! command -v git > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs dev-vcs/git"
        fi
        if ! command -v curl > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs net-misc/curl"
        fi
        if ! command -v parallel > "$NULL" 2> "$NULL" || ! is_gnu_parallel
        then
          pkgs="$pkgs sys-process/parallel"
        fi
        [ "$DEBUG" -eq 1 ] > "$NULL" 2> "$NULL" && __EMERGE_VERBOSE="-v" || __EMERGE_VERBOSE="-q"
        # shellcheck disable=SC2086
        [ "$pkgs" != "" ] && ACCEPT_KEYWORDS="~*" MAKEOPTS="-j$(($(nproc)+1))" $SUDO emerge $__EMERGE_VERBOSE $pkgs
        unset __EMERGE_VERBOSE
        ;;
      suse|opensuse|opensuse-leap|opensuse-tumbleweed)
        if ! command -v make > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs make"
        fi
        if ! command -v stow > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs stow"
        fi
        if ! command -v git > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs git"
        fi
        if ! command -v curl > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs curl"
        fi
        if ! command -v tar > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs tar"
        fi
        if ! command -v gzip > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs gzip"
        fi
        if ! command -v which > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs which"
        fi
        if ! command -v parallel > "$NULL" 2> "$NULL" || ! is_gnu_parallel
        then
          pkgs="$pkgs gnu_parallel"
        fi
        # shellcheck disable=SC2086
        [ "$pkgs" != "" ] && $SUDO zypper install -y $pkgs

        # add packman repositories (not necessary for my dots but useful for multimedia packages, codecs, etc):
        # https://en.opensuse.org/Additional_package_repositories
        # https://opensuse-community.org/
        if [ "$OS" != "suse" ]
        then
            if ! zypper lr -n packman > "$NULL" 2>&1
            then
                $SUDO rpmkeys --import - <<EOF
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQILBEUO9zgBEADYs3vI+Uy8XDpCDHAHj/fDxy0WDyE2sJYxhtnt+8h/xDW9HMo3
6IxoqtKfud21hmhcMRDFFNeZhuSDsJog3UuEa/SDyeHI50YU92FsBfyWITWC26UJ
YPfhzEt0fiU2xHUYXluuoOlkbIpHj9/ORFv65NMOx0bAFU4s9lvkLfOaLKuPqr8h
tpKPoa8v8rJYu1YUCZ6BOs06K9prz0l5xKUkGRVExCZe7VAMgNmZEhHxV5qT5NDM
31F8pVEvgGIVmEJbyytxfoPHaoQS0hIyLI2eL1H9wJf143X9qGP7bV6Nt94pEQJa
cG+6jjbBR4SbnJFKaWBlA/+r2JjpleYBYoE3XxWa4KxddTFJZmyomTVWIK/4rd/Y
+skZqDvbqKKn/P9kWvOH/u5knYb+hrzqdzo93VCvK+27U6LhE+Zt32J2dSXakdKD
U8PBIj87v7m6RwIVxrddR2fWpFx8/Iuoq7fU3VJKOsviNG9iQJjG60eE1CuD6dJP
zRIfRfCJApFwR7hTyPeVx4dHc0cpiGocMGXKCKau6IO+8Z2qEqZgupjm7KxSTF95
FL6tF+5LlDa6LsAnJAMV1KUAKMByzsBlUB7Zs32jegnHe+IkbSwXuyXjNGtjpr4M
8d1xYTUjD/8R8GkUh/kPPQ1bHIo1/w4n91+sgV5MV9xdhou0T1paIWG/CwAGKbQ2
UGFja01hbiBQcm9qZWN0IChzaWduaW5nIGtleSkgPHBhY2ttYW5AbGlua3MybGlu
dXguZGU+iEYEEBECAAYFAkcZBG4ACgkQnmpN6nKnxgixIwCcD42EFLyr5/AXj06n
KNykbppEOpAAn02GerIj2lIBOjmNHh+MWgwmvCLNiEoEEBECAAoFAkUROyQDBQJ4
AAoJEIhlNpbdr2RURT4AmwaatCKGsXcXYiKFXZMPODXkabOBAJwKU6CmK92OjDO6
rku4IHpCE8mSSYhvBBARAgAvBQJFDvnKBYMDwmRuAwUCeB6GPFtePl0rW0AuXVBh
Y2ttYW5cIHByb2plY3Q+JAAACgkQKWxsyjWmQTSNGACgmW+mzJqucqE9lYqXfSjO
ZeYloBMAoI3UatWQBuJJPi4g4uHk4i+Se/7fiQI6BBMBAgAkAhsDBgsJCAcDAgMV
AgMDFgIBAh4BAheABQJI1izgBQkHiZyoAAoJEEWh0GcavRr7NIoQAMP8I27Ud/5q
5ej46Gee8WzC6i4pLdRH2wPzx1iqHlzM2P41XeRMcfE67vSzsW6LVJy15DTUJF2D
3tynDSlWMWuonql0n/CjheKWBvxmnI/UyHOlZbXAW8EvUIvFRI6nor0PK+/2hKD7
d4UXZOtHaj6LlbS7uhy9BY4p40UqKHovdnuimKRv2nWMK1N4tyiUKJSoFmlrrRMX
bonCR4QtOM38gISs+BiKV2NJmDh30NxHrSa57Xilrxxnv0JbT5Rwsld9hH6Wpr0V
FROIbbyzEtX8QxpYxn+yTy+8Z/Ag9AxSRXRRSB2eY5seynCDCaNRCMzVd4YUEvVD
/n5Ebh0BgdCxGnZSygKcPUw+5ZFslqhj+rcNeBaDSbmmYD3eslbKroEnhh+VhWVF
reKAOK8JBDfiQpExGEYGPldPbmrbkwl8BFqk7tlmvkkoakrGwFrZYRMR0qTdvCtE
7fwo7MURjmdftNFW1VvUikTkfPltcEzBDYcn4Mw8gidNTm6uBTknHmI6JktAebFa
qZhiEZF/gESdlH6DLz/HfClO55C54wuM5x02APCHOiXQvfgOaikWXkJ8o0oObIrC
2JS9bdbNJ31HLNw0t/D+At8UHBTJllj/bLMnS97CVLeGI3rQJW81flCh1BsWNxgf
ERzwLlWHhqiTP7hLvFfjeO6TnnV2sB58iQI6BBMBAgAkAhsDBgsJCAcDAgMVAgMD
FgIBAh4BAheABQJMl8YMBQkPDZy/AAoJEEWh0GcavRr73/kP/inP7sKAl8UqL436
7ZVPvlcthBAqDkqBtZJJk9+Z2ew0+rPpi8z3MhRSQGm2Bobe/ut2+DUxjTSUP1I2
YOfBlMJ1gVUXtLcrvbd6SPSRP99usBTs3/oPHU6JUt4qe7lV0kqsreC1661gXVuZ
RRcqqpkqgJgjvQ4k03+JI/9Zemx9Cv60UpxSpm8CF+V1ENn3n/Onr0UDLf3NK14J
ZUkXeGjWIDxF58MWvX0TC1PA1oMF1H+Ae5oyvZkXUqZIcEe2uwHhrky2AJg3ZryD
JOosqaI4fYZv4HEWduRB+w8HAN3Bu5Xm1OqMR1EAerf3RZe4NGyKysDZlvK0mUod
sewzfIK3PkuSMpnJhSwJhE5XwwX7jFJB7CZ2BA/l7ykYVnW5lL6h0Co0bDtVPw+V
5bQ/g+TPo23xvjZA14dLjstJHqCSheVl+sYuh9qulk1tfjCJ3AkXPUTZrcF+2VUj
/34lxWJN3HU9rb4qnizRJBVXbMKUuGBlSLB5xw+w1v3RdoZzvhG3/IIYQsKJZ4xh
9Awj+FTiwnDfyHO9QQV1R1/cmHf/1oIf6L2UCOpK39LTfuu7qlUalkyUufGoM8up
TRJTiiMO1cbigv/vwYDGiuu8fLyOKF3mmDenOemiJSGiUYQNzQ+3FHDnUaXytA1s
3R2oeUad6LXYgqIU5f9bduqFI/RUiQI6BBMBAgAkAhsDBgsJCAcDAgMVAgMDFgIB
Ah4BAheABQJUF2WYBQkh1HE5AAoJEEWh0GcavRr7NE8P/iVjKG/6qHVcFdW1gZPQ
5HOw2W01urUg9Wl6UsG1ewk6I6s8i6OFIw+vuuOyri4PLr3S3wuaZ9zRIcZH8TDW
0TaqQ2sTnR7k1rLbTGu2sZcw2hllqgp9bhpFUH9aaahro4+rYw5BWt/n1my+LKeU
gOARMhdskb5PDfe4q83SHOA/aFkj/bZSAsEsdsxQfivAAldGc6C4IvyCnizwK6VN
iX5l72e1k2M8+Ivpj+DGVeyuIpejle1EHfgwLeKvch2fO0Cz3I2H/LO4j+RNnpIi
T5dzkOt9meuwu78hmpRA1xtNWUAypIGuPJsar94IesJDehgfbou1tOC6jE8R57Bw
UXLSJwBrN/mspV3BmRicsTiB3b+PiD14Tvezc+suWoVsu3alHg6ScE3c9YUSgql+
kbdOrxbs1uA0Hb7YO6Lokfah9BTdl3D+bpkMBFGVelUet4a8USPeCzH0jz2Jfz/o
rmo739eAze5TqrA2nxaHCiPZMcFJhUVw/imGFnKZg05rQp4gXk/Q+BCuKSk7NY9Z
z4o8giJmu6ALSbTTpo8cnxO1K3U3RuXwqbMmizp0DjdcGUw27/li+0cp8diM7y4O
5jWIE6OajEy6Cd4aMoc366Cl/ZZDvJer50tibgo4gpkFE2tyVMJ/1Fn5JbLpcw1f
NQ+FL4hW0OeTvsTDtqmGtIXaiQI6BBMBAgAkBQJFDvc4AhsDBQkDwmcABgsJCAcD
AgMVAgMDFgIBAh4BAheAAAoJEEWh0GcavRr7cLUP/0vPNt3wAeDkm1td8fuyV4e4
XaORDQYGPqssgxZcx9o0laIW1NQH7/+FXhPwYJjd08lAOTvM/O64IFfSfnB9BhX6
nSgsBSLAu737g1aUeDe+gSk/qajsB9I356Czx8qXQfMYqsSfCIL9l7C8QKCv6x2b
KBAQC5LBbI0zVdpKLgy/WANapI62bcbKNp0A7zhE0nk9DytJ0Q3cE4P9qhEtp2LG
pg5AI8E1Re984F0WzjonVCvNBP1L28KDdShIm/Xy5OhMtKiRnCETLzTAjybp81Qh
5Ke+XvxFzXS6GXjA9AXZx6D5LKR1lcEVFbP9zQ+7YkqDhM7mN0+0Fr2bDRyO2yAL
dz1ZSh/y9H2bBlxYro9ev60Niic9dAQJVnBMOGUK7GOHFvGCfSbMV0EjN7nnBM6B
HdilocGhobiWHZyX3iJjBqt5XH7NuiO69jq1mA0bS4k5x8ek+yjWvqsduoXcgk6j
fX07AP2c+jibIMHZs2BilSXew1MaqRWH0F2TOTV9BNl4sE0oJjzEokMpwFhG/VjF
3+l4tqwrTiWUAQrNR19TZD9Z4DlqAt7iu6IOYzwwNhtIIdRw6K+4wrB1EHVNZ6GB
QpchtTgxyw5qsXxsLNT3lklmY1utOVrmco0kbjxyFpoLGPYKFAFl9pnpjMK592bc
KJOaDWtYWPZUU/Q2qiNt
=Jx3Q
-----END PGP PUBLIC KEY BLOCK-----
EOF
            fi
        fi
        case "$OS" in
          opensuse-tumbleweed)
            if ! zypper lr -n packman > "$NULL" 2>&1
            then
                $SUDO zypper ar -cfp 90 http://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/ packman
                $SUDO zypper dup --allow-vendor-change --no-confirm --from packman
            fi
            ;;
          opensuse|opensuse-leap)
            __VERSION__="$(grep -E '^VERSION="[0-9.]+"$' /etc/os-release | sed 's/^VERSION=//g')"
            case "$__VERSION__" in
              '"15.0"')
                $SUDO zypper ar -cfp 90 http://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Leap_15.0/ packman
                $SUDO zypper dup --allow-vendor-change --no-confirm --from packman
              ;;
              '"42.3"')
                $SUDO zypper ar -cf http://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Leap_42.3/ packman
                $SUDO zypper dup --allow-vendor-change --no-confirm --from packman
              ;;
            esac
            ;;
        esac
        ;;
      void)
        if ! command -v make > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs make"
        fi
        if ! command -v stow > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs stow"
        fi
        if ! command -v git > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs git"
        fi
        if ! command -v stow > "$NULL" 2> "$NULL"
        then
          pkgs="$pkgs curl"
        fi
        if ! command -v parallel > "$NULL" 2> "$NULL" || ! is_gnu_parallel
        then
          pkgs="$pkgs parallel"
        fi
        # shellcheck disable=SC2086
        [ "$pkgs" != "" ] && $SUDO xbps-install -S -y $pkgs
        ;;
      arch|*)
        [ -z "$OS_LIKE" ] && OS_LIKE="$(grep -E '^ID_LIKE=' /etc/os-release | sed -e 's/^.*=//g' -e 's/\"//g')"
        [ "$OS" = "arch" ] && OS_LIKE="arch"
        case "$OS_LIKE" in
          arch|archlinux)
            pacman_install
            ;;
          debian)
            apt_install
            ;;
          *)
            printf "WARNING: Unable to bootstrap system!\\nContinuing at your peril!\\nOS: %s\\nOS_LIKE: %s\\n" "$OS" "$OS_LIKE" > /dev/stderr
            ;;
        esac
        ;;
    esac
    ;;

  CYGWIN*|MINGW32*|MSYS*)
    #God help you...
    # msys2
    if command -v pacman > "$NULL" 2> "$NULL"
    then
      SUDO="command" # Don't use sudo in msys2
      pacman_install
    fi
    ;;

  *)
    #some fancy BSD?
    _OS="$(uname -s)"
    if [ "$_OS" = "FreeBSD" ]
    then
      $SUDO pkg install -y git bash parallel gmake stow
    fi
    make="gmake"; export make;
    ;;
esac

RET="$?"

if [ -n "$CHECK_DEPS_ONLY" ]
then
  exit "$RET"
fi

export XDG_DATA_HOME="${XDG_DATA_HOME:-$HOME/.local/share}"
N_PREFIX="$XDG_DATA_HOME/n"; export N_PREFIX;
PATH="$XDG_DATA_HOME/n/bin:$PATH"; export PATH;

case "$(uname -m)" in
  aarch64)
    NODE_ARCH=arm64
    ;;
  armv7*)
    NODE_ARCH=armv7l
    ;;
  armv6*)
    NODE_ARCH=armv6l
    ;;
  amd64|x86_64|x64)
    NODE_ARCH=x64
    ;;
  i386|i686|x86)
    NODE_ARCH=x86
    ;;
  ppc|powerpc|ppc64|ppcle|ppc64le)
    NODE_ARCH=ppc64
    ;;
  *)
    NODE_ARCH=x64
esac

export NODE_ARCH;

if [ "$(make --version | head -1 | sed 's/^GNU Make .*/GNU Make/g')" != "GNU Make" ]
then
  if ! command -v gmake > "$NULL" 2> "$NULL"
  then
    printf "Please install GNU Make\\n" > /dev/stderr
    exit 1
  fi
  make="gmake"; export make;
else
  make="make"; export make;
fi

# shellcheck disable=SC2068
$make -d DEBUG=1 $@

[ -n "$CI" ] && exit 0

if [ -z "$1" ] || [ "$1" = "dotfiles" ]
then
  ./git_repos.sh
fi

exit 0
